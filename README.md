Responsive Web Design
===

The plain text source files for my book, *[Build a Better Web With Responsive Web Design](https://longhandpixels.net/books/responsive-web-design)*

# Contributing

I welcome contributions of all kinds from readers. I am incredibly grateful for the feedback so first and foremost let me say thank you just for *wanting* to contribute; you're awesome.

If you've found a typo or other technical error you can just [open an issue](https://bitbucket.org/longhandpixels/responsive-design-book/issues/new) and tell me about the problem (easiest method). If you're feeling more ambitious, you can fork this repository, make the corrections/changes yourself and issue a pull request. 
If you don't have a BitBucket account and would prefer not to create one, you can email your thoughts/corrections to scott at longhandpixels.net.

If you have a more substantial edit, correction or rewrite in mind, please [open an issue](https://bitbucket.org/longhandpixels/responsive-design-book/issues/new) so we can discuss it.

## License

Although these files are freely available on BitBucket, the book *Build a Better Web With Responsive Web Design* is still copyright Scott Gilbertson. Contributors will be noted by BitBucket username in the preface. Be sure that you are okay with that before contributing.


## Formatting

The files in this repository are all written in [Markdown](http://daringfireball.net/projects/markdown/), specifically [Pandoc](http://johnmacfarlane.net/pandoc/)-flavored Markdown. Please at least skim the [Pandoc markdown documentation](http://johnmacfarlane.net/pandoc/README.html#pandocs-markdown) before making major edits to the text. Properly formatting your changes greatly increases the chances they will be approved.

That said, I don't use a ton of Markdown in the book. It is, after all, mostly just text. The main thing to be aware of is that I use `~~~` to denote blocks of code. It looks like this:

```
    ~~~.language-<classname>
       ...code...
    ~~~
```

Where `<classname>` is the name of the language being used, e.g. `language-markup` or `language-css` (necessary for styling the PDF and HTML versions of the book).

For the purposes of sanity I also use inline links and footnotes rather than reference style. Otherwise it's pretty standard markdown. If you're unsure about how to format something [open an issue](https://bitbucket.org/longhandpixels/responsive-design-book/issues/new).


## FAQS

### What the @?!#? It's all one giant text file?

Yup. How about that? It's the easiest way for me to work. I can jump around very quickly thanks to the excellent code folding feature in my text editor. I use Vim with the [vim-markdown-folding](https://github.com/nelstrom/vim-markdown-folding) plugin, but Emacs, Sublime, BBEdit and others likely offer the same functionality.

### I found an error in the example files what should I do?

For now I'm leaving the sample files out of the repo so just email me: scott at longhandpixels.net

### There's a typo in the case studies

No there isn't. Just kidding. Again, email me.

### Are you worried no one will buy your book because it's all here for free?

Honestly, a little bit. Though I think that reading the text file is a wretched experience compared to the purchased version, which is available in five formats and works on every device I've ever tested it on. And of course there's no video content, sample files, case studies or any of the other great extras.

In the end it seems more interesting to try sharing the source with the world. I believe this is the future of books, especially technical books. There will be a raw "source" version with a "compiled" version for sale. Not all that different from software really. If you'd like to support my efforts and make it possible for me to write more books, consider picking up a copy of *[Build a Better Web With Responsive Web Design](http://longhandpixels.net/responsive-web-design)*.
